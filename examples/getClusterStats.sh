# Script Pre-requisites
# 
# Run export vmtUser="YourUsername" prior to running this script
# Run export vmtPass="YourPassword" prior to running this script
# Run plans under the vmtUser account and wait for them to complete

vmtInstances="$*"
[[ "${vmtInstances}" == "" ]] && vmtInstances="localhost"
for vmtHost in ${vmtInstances}
do
  export vmtHost
  . ./vmtRESTlib.sh ${vmtHost}
  marketName=$(getUserUUIDByName ${vmtUser})_BasePlan
  pmUUIDs=($(getPMUUIDs ${marketName}))
  echo "$(getClusterHostStates ${marketName} pmUUIDs)"
done | sort | uniq
