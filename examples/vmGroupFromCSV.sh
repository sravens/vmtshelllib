#!/bin/sh
#
# Author: simon.ravenscroft@vmturbo.com
# Date: FEB 20 2015
#
# Script to sync VMTurbo group with CSV file members
#
# Always save this script with 700 permissions

[[ "${VMTDEBUG}" != "" ]] && DEBUG=1

# Load Shell Function Libraries
[[ "${VMTLIBPATH}" == "" ]] && VMTLIBPATH=/root/scripts
scriptName=$(basename $0)
scriptName="${scriptName%.*}"
. ${VMTLIBPATH}/vmtCommonlib.sh ${scriptName}
. ${VMTLIBPATH}/vmtRESTlib.sh ${scriptName}

function usage() { 
logDebug "${scriptName} $*"
logDebug "[$key][$value]"
cat <<ZaP 1>&2
${ttyGreen}Usage: ${scriptName} -u username -p password -g groupName -c column -h hostname -t N -f csvFile${ttyReset}
 -c column	The column number (starting at 1) containing the VM name (default 1) 
 -t N		Ignore the first N lines of the CSV file (default 0)
 -h hostname	Hostname of the VMTurbo system (default localhost)
 -u username	A valid administrator VMTurbo user (default administrator)
 -p password	Password for the user
 -g groupName	Name of the Group to create (default csvfilename)
 -f csvFile	The CSV file containing the VM names to place in the group
ZaP
exit
}

function parseOptions() {
# set defaults
topLines=0
columnNum=1
csvFile=""
groupName=""
args=" $*"
key=""
while [[ ${#args} -gt 1 ]]; do
        prevKey="${key}"
        key="${args%% -+}"; key="${key%% *}"
        value="${args%% -+}"; value="${value#* }"; value="${value%% -*}"
        [[ "${key}" == "" ]] && args="${args#* -}"
        [[ "${key}" == "" ]] && continue
        [[ "${key}" == "${prevKey}" ]] && break
        args="${args#* -}"
      	case "${key}" in
        	"c"	) columnNum="${value}"; 
			  [[ "${columnNum}" =~ ^[0-9]+$ ]] || usage;;
		"t"	) topLines="${value}";;
        	"h"	) vmtHost="${value}";;
        	"u"	) vmtUser="${value}";;
        	"p"	) vmtPass="${value}";;
        	"g"	) groupName="${value}";;
        	"f"	) csvFile="${value}"; 
		  	  [[ "${csvFile}" == "" ]] && usage;
		  	  [[ ! -f "${csvFile}" ]] && logError "CSV File not found at \"${csvFile}\"";;
        	*	) usage;;
      	esac
done
#fi
# Default groupName to filename if not specified
if [[ "${groupName}" == "" ]]; then
	groupName="${csvFile##*/}"; groupName="${groupName%.*}"
fi
[[ "${groupName}" == "" ]] && usage
maxCols=$(head -n 1 "${csvFile}" | sed 's/\,/\n/g' | wc -l)
[[ "${columnNum}" -gt "${maxCols}" ]] && logError "ColumnNum (${columnNum}) is greater than number of columns in CSV file (${maxCols})"
result=$(getMarketNames)
[[ "${result}" == "" ]] && logError "Authentication failed for user "${vmtUser}""
}

#### MAIN

parseOptions "$*"

tmpUnmatchedVMs=/tmp/"${scriptName%.*}"_${groupName// /_}_unmatched.txt; > ${tmpUnmatchedVMs}
tmpMatchedVMs=/tmp/"${scriptName%.*}"_${groupName// /_}_matched.txt; > ${tmpMatchedVMs}
tmpClusterNames=/tmp/"${scriptName%.*}"_${groupName// /_}_clusters.txt; > ${tmpClusterNames}

logInfo "Using group \"${groupName}\" and adding members from CSV file \"${csvFile}\""

# Iterate over the CSV list
# Add members to the group
vmUUIDS=()
tail -n +${topLines} "${csvFile}" | cut -d',' -f ${columnNum} > /tmp/crud
while read vmName
do
	#vmName+=".rz.ch.zurich.com"
	vmUUID=$(getVMUUID "${vmName}")
	if [[ "${vmUUID}" != "" ]] ; then
		logDebug "Found UUID [${vmUUID}] for VM named [${vmName}]"
		vmUUIDS+=(${vmUUID})
		printf "${vmName}\n" >> "${tmpMatchedVMs}"
	else
		printf "${vmName}\n" >> "${tmpUnmatchedVMs}"
	fi
done < /tmp/crud
logDebug "vmUUIDS=${vmUUIDS[@]}"
groupUUID=$(createVMGroupWithMembers "${groupName}" vmUUIDS)
# Report failures
matched=$(wc -l ${tmpMatchedVMs} | cut -d' ' -f1)
unmatched=$(wc -l ${tmpUnmatchedVMs} | cut -d' ' -f1)
logInfo "Group \"${groupName}\" now has ${matched} members from CSV file \"${csvFile}\""
[[ ${unmatched} -gt 0 ]] && logWarn "${unmatched} CSV file entries were UNMATCHED. Report is at \"${tmpUnmatchedVMs}\""

#cleanup
exit 0
