#!/bin/sh
#
# Author: simon.ravenscroft@vmturbo.com
# Date: FEB 20 2015
#
# Implemens a Shell Function Library for Common functions
#

DEBUG=0
[[ "${VMTDEBUG}" != "" ]] && DEBUG=1

ttyRed=$(tput setaf 1)
ttyGreen=$(tput setaf 2)
ttyDim=$(tput dim)
ttyReset=$(tput sgr0)

function logDebug() {
	if [ ${DEBUG} -eq 1 ]; then
        	timeStamp=$(/usr/bin/date -u +"%F %T")
        	printf ${ttyDim}"[${timeStamp}] [DEBUG] $*${ttyReset}\n"
	fi
}

function logInfo() {
        timeStamp=$(/usr/bin/date -u +"%F %T")
        printf "[${timeStamp}] [INFO] ${ttyGreen}$*${ttyReset}\n"
}

function logWarn() {
        timeStamp=$(/usr/bin/date -u +"%F %T")
        printf "[${timeStamp}] [WARN] ${ttyRed}$*${ttyReset}\n"
}

function logError() {
        timeStamp=$(/usr/bin/date -u +"%F %T")
        printf "[${timeStamp}] [ERROR] ${ttyRed}$*${ttyReset}\n"
        exit -1
}

function init() {
	return
}

function cleanup() {
	/usr/bin/rm -f /tmp/"${0%.*}"_$$_*
}

init
return 0
