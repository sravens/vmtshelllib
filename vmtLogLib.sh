#!/bin/sh
#
# Author: simon.ravenscroft@vmturbo.com
# Date: MAY 05 2015
#
# Implements a Shell library for processing VMTurbo Log data
#

DEBUG=0
[[ "${VMTDEBUG}" != "" ]] && DEBUG=1
[[ -f "/var/log/tomcat/catalina.out" ]] && VMTLOGPATH="/var/log/tomcat"
[[ -d "/srv/tomcat/data/version" ]] && VMTAPPPATH="/srv/tomcat"
[[ "${vmtLogFile}" == "" ]] && vmtLogFile="${VMTLOGPATH}/catalina.out"

function init() {
	return
}

function cleanup() {
	/usr/bin/rm -f /tmp/"${0%.*}"_$$_*
}

function countMoves() {
	regex="$*"
	egrep "^.*\[Control\].*MOVE.*"${regex}".*$" ${VMTLOGPATH}/catalina.out
}

init
return 0
