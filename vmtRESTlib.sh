#!/bin/sh
#
# Author: simon.ravenscroft@vmturbo.com
# Date: FEB 20 2015
#
# Implements a REST API Shell function library
#
# Always save this script with 700 permissions owned by root
#
# Load the library using . vmtRESTlib.sh $0

[[ "${VMTLIBPATH}" == "" ]] && VMTLIBPATH=/root/scripts
#. ${VMTLIBPATH}/vmtCommonlib.sh $0

[[ "${vmtUser}" == "" ]] && vmtUser="administrator"
[[ "${vmtPass}" == "" ]] && vmtPass="administrator"
[[ "${vmtHost}" == "" ]] && vmtHost="localhost"

tmpEntities=/tmp/"${1%.*}"_$$_entities.xml; > ${tmpEntities}
tmpGroups=/tmp/"${1%.*}"_$$_groups.xml; > ${tmpGroups}
tmpRESToutput=/tmp/"${1%.*}"_$$_rest.out; > ${tmpRESToutput}
#noproxy="--noproxy localhost"

function vmtRESTlibHelp {
cat << ZAP
function getMarketNames()
function getMarketNamesByUserName()
function getVMUUID()
function getPMUUID()
function getHostState()
function getPMDisplayNames()
function getPMUUIDs()
function countPMs()
function countActivePMs()
function countSuspendedPMs()
function getUserUUIDByName()
function countGroup()
function getGroupUUIDByName()
function createVMGroup()
function deleteGroupByName()
function addGroupMember()
function createGroupWithMembers()
function removeVMGroupMemberByUUID()
function clearVMGroup()
function createScopedPlan()
function createPlan()
function deletePlan()
function getPlanUUID()
function getPlanState()
function setPlanScope()
function setPlanState()
function runPlan()
function stopPlan()
function waitOnPlan()
function getHostCluster()
function getClusterHostStates()
function getHostClusters()
ZAP
}

function init() {
# cache some of the heavier items to save on the number of REST calls
        curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/Market/entities > ${tmpEntities}
        curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups > ${tmpGroups}
}

function cleanup() {
        [[ "${DEBUG}" -ne 1 ]] && rm -f /tmp/"${0%.*}"_$$_*
}

# getMarketNames
function getMarketNames() {
        curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*name="\([^"]\+\).*/\1/p'
}

# getMarketDisplayNamesByUserName userName
function getMarketNamesByUserName() {
        userUUID=$(getUserUUIDByName ${1})
        curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*name="\([^"]\+\).*/\1/p' | grep "${userUUID}"
}

# getVMUUID displayName
function getVMUUID() {
        grep -i "displayName=\"$*\"" ${tmpEntities} | /usr/bin/sed -n '/VirtualMachine/s/.*uuid="\([^"]\+\).*/\1/p'
}

## HOST functions
# getPMUUID displayName
function getPMUUID() {
        grep -i "displayName=\"$*\"" ${tmpEntities} | /usr/bin/sed -n '/PhysicalMachine/s/.*uuid="\([^"]\+\).*/\1/p'
}


# getHostState market pmUUID
function getHostState() {
        marketName="${1}"
        [[ "${marketName}" == "" ]] && marketName="Market"
        pmUuid="${2}"
        /usr/bin/curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName}/entities/${pmUuid}?property=state \
                | /usr/bin/sed -n '/\<ServiceEntity\>/s/.*displayName="\([^"]\+\).*state="\([^"]\+\).*/\1,\2/p'
}

# getPMs [marketName]
function getPMDisplayNames() {
        marketName="${1}"
        [[ "${marketName}" == "" ]] && marketName="Market"
        curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName}/entities?classname=PhysicalMachine \
        | /usr/bin/sed -n '/\<ServiceEntity\>/s/.*displayName="\([^"]\+\).*/\1/p' | sort | uniq
}

# getPMUUIDs [marketName]
function getPMUUIDs() {
        marketName="${1}"
        [[ "${marketName}" == "" ]] && marketName="Market"
        curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName}/entities?classname=PhysicalMachine \
        | /usr/bin/sed -n '/\<ServiceEntity\>/s/.*uuid="\([^"]\+\).*/\1/p' | sort | uniq
}

# countPMs marketName pmUuids[]
function countPMs(){
        marketName="${1}"
        name=$2[@]
        pmUuids=("${!name}")
        for pmUuid in ${pmUuids[@]}
        do
                /usr/bin/curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName}/entities/${pmUuid}?property=state \
                | /usr/bin/sed -n '/\<ServiceEntity\>/s/.*displayName="\([^"]\+\).*state="\([^"]\+\).*/\1 \2/p'
        done | sort | uniq | wc -l
}

# countActivePMs marketName pmUuids[]
function countActivePMs(){
        marketName="${1}"
        name=$2[@]
        pmUuids=("${!name}")
        for pmUuid in ${pmUuids[@]}
        do
                /usr/bin/curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName}/entities/${pmUuid}?property=state \
                | /usr/bin/sed -n '/\<ServiceEntity\>/s/.*displayName="\([^"]\+\).*state="\([^"]\+\).*/\1 \2/p'
        done | grep ACTIVE | sort | uniq | wc -l
}

# countSuspendedPMs marketName pmUuids[]
function countSuspendedPMs(){
        marketName="${1}"
        name=$2[@]
        pmUuids=("${!name}")
        for pmUuid in ${pmUuids[@]}
        do
                /usr/bin/curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName}/entities/${pmUuid}?property=state \
                | /usr/bin/sed -n '/\<ServiceEntity\>/s/.*displayName="\([^"]\+\).*state="\([^"]\+\).*/\1 \2/p'
        done | grep SUSPEND | sort | uniq | wc -l
}

## USER FUNCTIONS
# getUserUUIDByName userName
function getUserUUIDByName() {
        userName="${1}"
        if [[ "${userName}" != "" ]] ; then
                /usr/bin/curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/users \
                | /usr/bin/sed -n '/\<TopologyElement\>/s/.*name="'${userName}'".*uuid="\([^"]\+\).*/\1/p'
        fi
}

## GROUP FUNCTIONS
# countGroup displayName
function countGroup() {
        getGroupUUIDByName "$*" | wc -l
}


# getGroupUUID displayName
function getGroupUUIDByName() {
        grep "displayName=\"$*\"" ${tmpGroups} | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

# createVMGroup groupDisplayName
function createVMGroup() {
        /usr/bin/curl ${noproxy} -skX POST https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups \
                        -d groupName="$*" -d seType=VirtualMachine -d reportEnabled=true -d static=true \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

# createPMGroup groupDisplayName
function createPMGroup() {
        /usr/bin/curl ${noproxy} -skX POST https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups \
                        -d groupName="$*" -d seType=PhysicalMachine -d static=true \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

# deleteGroupByName groupDisplayName
function deleteGroupByName() {
        groupUUIDs=$(getGroupUUIDByName "$1")
        logDebug "deleteGroupByName: Group UUIDS to delete: ${groupUUIDs}"
        for groupUUID in ${groupUUIDs}
        do
                if [ "${groupUUID}" != "" ]; then
                        /usr/bin/curl ${noproxy} -skX DELETE https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups/${groupUUID} \
                        | /usr/bin/tee ${tmpRESToutput} \
                        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
                fi
        done
}

# addGroupMember groupUUID memberUUID
function addGroupMember() {
        /usr/bin/curl ${noproxy} -skX POST https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups/$1/entities/$2 2>&1 \
        | /usr/bin/tee ${tmpRESToutput} \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

# createVMGroupWithMembers groupName memberUUID[]
function createVMGroupWithMembers() {
	groupName="${1}"; shift
        $(createGroupWithMembers VirtualMachine ${groupName} ${@})
}

# createPMGroupWithMembers groupName memberUUID[]
function createPMGroupWithMembers() {
	groupName="${1}"; shift
        $(createGroupWithMembers PhysicalMachine ${groupName} ${@})
}

# createGroupWithMembers entityType groupName memberUUID[]
function createGroupWithMembers() {
        entityType="${1}"; shift
        groupName="${1}"; shift
        memberUUIDs=( "${@}" )
        outString="groupName=${groupName}&seType=${entityType}&reportEnabled=true&static=true"
        for memberUUID in ${memberUUIDs[@]}
        do
                outString="${outString}&uuidsList[]=${memberUUID}"
        done ; echo ${outString} \
        | /usr/bin/curl ${noproxy} -skX POST -d @- https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups \
        | /usr/bin/tee ${tmpRESToutput} \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

# removeVMGroupMemberByUUID groupUUID groupMemberUUID
function removeVMGroupMemberByUUID() {
        /usr/bin/curl ${noproxy}        -skX DELETE https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups/$1/entities/$2 \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

# clearVMGroup displayName
# empties the group of members, does not remove the group
function clearVMGroup() {
        groupUUID=$(getGroupUUID "$1")
        if [ "${groupUUID}" != "" ]; then
                /usr/bin/curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups/${groupUUID}/entities \
                | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p' \
                | while read memberUUID
                do
                        logDebug "Removing existing group member [${memberUUID}]"
                        result="$(removeVMGroupMemberByUUID "${groupUUID}" "${memberUUID}")"
                done
        fi
}

# createScopedPlan name groupuuid[]
# creates a plan or returns the uuid of one if it already exists
function createScopedPlan() {
        marketname="${1}"
        name=$2[@]
        groupuuids=("${!name}")
        paramstring=""
        for groupuuid in ${groupuuids[@]}
        do
                [[ "${paramstring}" != "" ]] && paramstring += "&"
                paramstring="${paramstring}scope[]=${groupuuid}"
        done
        /usr/bin/curl ${noproxy} -sx post -d ${paramstring} https://${vmtuser}:${vmtpass}@${vmthost}/vmturbo/api/markets/${marketname} \
        | /usr/bin/sed -n '/\<topologyelement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

# createPlan
# creates a plan or returns the uuid of one if it already exists
function createPlan() {
        if [[ "$*" != "" ]] ; then
                if [[ "$1" != "Market" ]] && [[ "$1" != "Market_Default" ]] ; then
                        /usr/bin/curl ${noproxy}   -skX POST https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/$1 \
                        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
                else
                        logWarn "Market [$1] is not allowed, try another name"
                fi
        else
                logError "createPlan: Nothing to do!"
        fi
}


# deletePlan market
function deletePlan() {
        if [[ "$*" != "" ]] ; then
                if [[ "$1" != "Market" ]] && [[ "$1" != "Market_Default" ]] ; then
                        /usr/bin/curl ${noproxy}   -skX POST https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/$1 \
                        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
                else
                        logWarn "Market [$1] is not allowed, try another name"
                fi
        else
                logError "createPlan: Nothing to do!"
        fi

}


# getPlanUUID
function getPlanUUID() {
        if [[ "$*" != "" ]] ; then
                /usr/bin/curl ${noproxy}   -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/$1 \
                | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
        else
                logError "createPlan: Nothing to do!"
        fi
}

# getPlanState
function getPlanState() {
        if [[ "$*" != "" ]] ; then
                /usr/bin/curl ${noproxy}   -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/$1 \
                | /usr/bin/sed -n '/\<TopologyElement\>/s/.*state="\([^"]\+\).*/\1/p'
        else
                logError "createPlan: Nothing to do!"
        fi
}

# setPlanScope planName groupName
function setPlanScope() {
        if [[ "$*" != "" ]] ; then
                if [[ "$1" != "Market" ]] && [[ "$1" != "Market_Default" ]] ; then
                        planUUID=$(getPlanUUID "$1")
                        if [[ "${planUUID}" != "" ]] ; then
                                groupUUID=$(getGroupUUID "$2")
                                if [[ "${groupUUID}" != "" ]] ; then
                                        /usr/bin/curl ${noproxy}   -skX POST https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/$1 --data scope[]=${groupUUID} \
                                        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*state="\([^"]\+\).*/\1/p'
                                else
                                        logWarn "setPlanScope: Couldn't get UUID for group with name [$2]"
                                fi
                        else
                                logWarn "setPlanScope: Couldn't get UUID for plan with name [$1]"
                        fi
                else
                        logWarn "Market [$1] is not allowed, try another name"
                fi
        else
                        logError "createPlan: Nothing to do!"
        fi
}

# setPlanState planName state
# state = run | stop
function setPlanState() {
        if [[ "$*" != "" ]] ; then
                marketName="$1"; state="$2"
                if [[ "${marketName}" != "Market" ]] && [[ "${marketName}" != "Market_Default" ]] ; then
                        planUUID=$(getPlanUUID "${marketName}")
                        if [[ "${planUUID}" != "" ]] ; then
                                if [[ "${state}" == "run" ]] || [[ "${state}" == "stop" ]] ; then
                                        logDebug "/usr/bin/curl ${noproxy}   -skX POST https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName} --data state=${state}"
                                        /usr/bin/curl ${noproxy} -skX POST https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName} --data state=${state} \
                                        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*state="\([^"]\+\).*/\1/p'
                                else
                                        logWarn "setPlanScope: State [${state}] must be run|stop"
                                fi
                        else
                                logWarn "setPlanScope: Couldn't get UUID for plan with name [${marketName}]"
                        fi
                else
                        logWarn "Market [${marketName}] is not allowed, try another name"
                fi
        else
                        logError "setPlanScope: Nothing to do!"
        fi
}

# runPlan planName
function runPlan() {
        setPlanState ${1} run
}

# stopPlan planName
function stopPlan() {
        setPlanState ${1} stop
}

# waitOnPlan planName
function waitOnPlan(){
        marketName="${1}"
        while [[ "$(getPlanState ${marketName})" == "RUNNING" ]]
        do
                sleep 1
        done
        $(getPlanState ${marketName})
}

# getHostCluster market pmUUID
function getHostCluster() {
        if [[ "$*" != "" ]] ; then
                marketName="${1}"
                pmUUID="${2}"
                clusterUUID=$(/usr/bin/curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/markets/${marketName}/entities/${pmUUID}/services | /usr/bin/sed -n '/\<Commodity\>.*displayName="ClusterCommodity\//s/.*key="Cluster::\([^"]\+\).*/\1/p')
                /usr/bin/curl ${noproxy} -skX GET  https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/topology/${clusterUUID} | /usr/bin/sed -n '/\<TopologyElement\>/s/.*displayName="\([^"]\+\).*/\1/p'
        fi
}

# getClusterHostStates market pmUUIDs
function getClusterHostStates() {
        marketName="${1}"
        name=$2[@]
        pmUuids=("${!name}")
        for pmUuid in ${pmUuids[@]}
        do
                clusterHostStates+="${vmtHost},$(getHostCluster ${marketName} ${pmUuid}),$(getHostState ${marketName} ${pmUuid})"
        done
        echo "${clusterHostStates}"
}

# getHostCluster
function getHostClusters() {
        /usr/bin/curl ${noproxy}   -skX GET  https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/topology?classname=Cluster \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

## Rudimentary Test Run
#
testRun() {
marketName="testMarket"
userName="administrator"
set -x
        #clusterUUIDs=$(getHostClusters)
        #marketIndex=1
        #for clusterUUID in $(getHostClusters)
        #do
                #groupUUIDs=()
                #groupUUIDs+=${clusterUUID}
                #planUUID=$(createScopedPlan ${marketName}${marketIndex} groupUUIDs)
                #marketIndex+=1
        #done
        markets=$(getMarketNamesByUserName ${userName})
        #hostUUID=$(getPMUUID SimonDEMO)
        #groupUUID=$(getGroupUUIDByName DemoVMs)
        #groupUUIDs=()
        #groupUUIDs+=${groupUUID}
        #planUUID=$(createScopedPlan myMarket3 groupUUIDs)
        #planStatus=$(getPlanState myMarket3)
        #$(runPlan myMarket3)
        #planStatus=$(waitOnPlan myMarket3)
        #echo $(getPMDisplayNames myMarket3)
        marketName=$(getUserUUIDByName ${userName})_BasePlan
        pmUUIDs=($(getPMUUIDs ${marketName}))
        clusterStates=$(getClusterHostStates ${marketName} pmUUIDs)
# Test to create an intersection group from VC Annotations
#       intersectVMGroup test_vc_intersect LABEL1:VALUE1 LABEL2:VALUE2
}

function getGroupMembers() {
        /usr/bin/curl ${noproxy} -skX GET https://${vmtUser}:${vmtPass}@${vmtHost}/vmturbo/api/groups/$1/entities 2>&1 \
        | /usr/bin/tee ${tmpRESToutput} \
        | /usr/bin/sed -n '/\<TopologyElement\>/s/.*uuid="\([^"]\+\).*/\1/p'
}

# intersectGroup entityType newGroupName groupNames[]
function intersectGroup() {
	entityType="$1"; shift
        newGroupName="$1"; shift
        groupNames=( "$@" )
        firstpass=1
        groupMemberUUIDs_1=()
        groupMemberUUIDs_2=()
        for groupName in "${groupNames[@]}"
        do
                groupUUID=$(getGroupUUIDByName ${groupName})
                groupMemberUUIDs_2=($(getGroupMembers ${groupUUID}))
                [ ${firstpass} ] && groupMemberUUIDs_1=${groupMemberUUIDs_2[*]} && unset firstpass
                intersectGroupMemberUUIDs=()
                l2=" ${groupMemberUUIDs_2[*]} "                 # add framing blanks
                for item in ${groupMemberUUIDs_1[@]}; do
                  if [[ $l2 =~ " $item " ]] ; then              # use $item as regexp
                    intersectGroupMemberUUIDs+=($item)
                  fi
                done
                groupMemberUUIDs_1=${intersectGroupMemberUUIDs[*]}
        done
	
        createGroupWithMembers ${entityType} ${newGroupName} ${intersectGroupMemberUUIDs[*]}
}

function intersectVMGroup() {
	newGroupName="${1}"; shift
        $(intersectGroup VirtualMachine ${newGroupName} ${@})
}

function intersectPMGroup() {
	newGroupName="${1}"; shift
        $(intersectGroup PhysicalMachine ${newGroupName} ${@})
}

# copyGroup sourceGroupName
function copyGroup() {
        groupUUID=$(getGroupUUIDByName "$1")
        groupMemberUUIDs=()
        groupMemberUUIDs=($(getGroupMembers ${groupUUID}))
        result=$(createVMGroupWithMembers $1 ${groupMemberUUIDs[@]})
}

## MAIN
#
init
[[ "$*" == "TEST" ]] && echo $(testRun)
